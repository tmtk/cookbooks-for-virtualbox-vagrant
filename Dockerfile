FROM centos

RUN yum install -y passwd
RUN yum install -y openssh
RUN yum install -y openssh-server
RUN yum install -y openssh-clients
RUN yum install -y sudo

# create user
RUN useradd core
RUN passwd -f -u core
RUN mkdir -p /home/core/.ssh; chown core /home/core/.ssh; chmod 700 /home/core/.ssh
ADD ./.ssh/authorized_keys /home/core/.ssh/authorized_keys
RUN chown core /home/core/.ssh/authorized_keys; chmod 600 /home/core/.ssh/authorized_keys

# Enable ssh login for CoreOS
ADD ./etc-pam.d-sshd /etc/pam.d/sshd

# setup sudoers
RUN echo "core    ALL=(ALL)       ALL" >> /etc/sudoers.d/core

# setup sshd and generate ssh-keys by init script
#ADD ./sshd_config /etc/ssh/sshd_config
RUN /etc/init.d/sshd start
RUN /etc/init.d/sshd stop

# expose sshd port
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
