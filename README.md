# README

Provide a fast way to launch [CoreOS](http://coreos.com/docs/using-coreos/),
which supports [Docker](http://www.docker.io/),
with [Vagrant](http://www.vagrantup.com/) on MacOSX.

Set up Virtualbox, Vagrant and fabric
-------------------------------------

    $ gem install berkshelf chef --no-ri --no-rdoc
    $ berks install --path=cookbooks
    $ sudo chef-solo -c solo.rb -j chef.json
    ...
    Chef Client finished, 3 resources updated

Set up CoreOS on guest OS
-------------------------

    $ vagrant up
    $ opts=`vagrant ssh-config | ./sshconf2opts.py guest-ssh`
    $ fab $opts docker_build
    ...
    Disconnecting from core@127.0.0.1:2222... done.
    $ fab $opts docker_restart
