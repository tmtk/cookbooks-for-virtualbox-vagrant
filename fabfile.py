from fabric.api import env, run, put, sudo, runs_once, task, execute

## Restart docker using systemd service file.
## see http://coreos.com/docs/using-coreos/

@task
def docker_restart():
  sudo("systemctl stop docker.service")
  put("_docker.service", "_docker.service")
  sudo("mv -f _docker.service /media/state/units")
  sudo("systemctl restart local-enable.service")
  sudo("systemctl start _docker.service")

@task
def docker_build():
  put("Dockerfile", "Dockerfile")
  put("etc-pam.d-sshd", "etc-pam.d-sshd")
  run("docker build -t core/sshd .")

@task
def put_vagrant_insecure_key(path):
  put(path, ".ssh/id_rsa")
  run("chmod 600 .ssh/id_rsa")
