log_level     :info
log_location  STDOUT

home = File.expand_path File.dirname __FILE__
cookbook_path ["#{home}/cookbooks"]
cache_options :path => "#{home}/.cache"
file_cache_path  "#{home}/.cache"
#role_path        "#{home}/.role"
