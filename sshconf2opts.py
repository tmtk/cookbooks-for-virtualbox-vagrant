#!/usr/bin/env python
## usage:
##   vagrant ssh-config | sshconf2fabopts.py fabric
##   vagrant ssh-config | sshconf2fabopts.py ssh
##   vagrant ssh-config | sshconf2fabopts.py IdentityFile
import os
import sys

if len(sys.argv) == 1:
  exit(1)

kv = dict([e.strip().split(" ") for e in sys.stdin])
if sys.argv[1] == "guest-fab":
  print "-i %s -u %s -H %s --port=%s" % (kv['IdentityFile'], kv['User'], kv['HostName'], kv['Port']),
elif sys.argv[1] == "guest-ssh":
  print "-i %s -l %s -p %s %s" % (kv['IdentityFile'], kv['User'], kv['Port'], kv['HostName']),
else:
  print kv[sys.argv[1]],
